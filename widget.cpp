#include "widget.h"

Widget::Widget(QWidget *parent)
    : QOpenGLWidget(parent)
{
    connect(&tm_,SIGNAL(timeout()),this,SLOT(slotTimeout()));
    tm_.start(30);
}

Widget::~Widget()
{

}

void Widget::initializeGL()
{
    render_.initsize(1.0,QImage("test.jpg"));
    lightLocation_ = QVector3D(50,10,0);
    camera_ = QVector3D(0,0,6);
}

void Widget::paintGL()
{
    QOpenGLExtraFunctions *f = QOpenGLContext::currentContext()->extraFunctions();
    f->glClearColor(0.0,0.0,0.0,1.0);
    f->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    QMatrix4x4 vMatrix;
    vMatrix.lookAt(camera_,QVector3D(0,0,-1),QVector3D(0,1,0));

    QMatrix4x4 mMatrix;
    mMatrix.rotate(30,1,0,0);
    mMatrix.rotate(angle_,0,1,0);
    mMatrix.translate(1.5,0,0);

    render_.render(f,pMatrix,vMatrix,mMatrix,lightLocation_,camera_);
    angle_ += 5;
}

void Widget::resizeGL(int w, int h)
{
    pMatrix.setToIdentity();
    pMatrix.perspective(45,float(w)/h,0.01f,100.0f);
}

void Widget::slotTimeout()
{
    update();
}
