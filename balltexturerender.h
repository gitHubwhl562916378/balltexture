#ifndef BALLTEXTURERENDER_H
#define BALLTEXTURERENDER_H

#include <QOpenGLExtraFunctions>
#include <QOpenGLTexture>
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>
#include <QImage>
#define PI 3.14159265f

class BallTextureRender
{
public:
    BallTextureRender() = default;
    ~BallTextureRender();
    void initsize(float r,QImage &img);
    void render(QOpenGLExtraFunctions *f,QMatrix4x4 &pMatrix,QMatrix4x4 &vMatrix,QMatrix4x4 &mMatrix,QVector3D &lightLocation,QVector3D &camera);

private:
    QOpenGLShaderProgram program_;
    QOpenGLTexture *texture_{nullptr};
    QOpenGLBuffer vbo_;
    float r_ = 0.0f;
    QVector<GLfloat> points_;
};

#endif // BALLTEXTURERENDER_H
