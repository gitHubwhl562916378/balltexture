#include "balltexturerender.h"

BallTextureRender::~BallTextureRender()
{
    if(texture_){
        texture_->destroy();
        delete texture_;
    }
}

void BallTextureRender::initsize(float r, QImage &img)
{
    program_.addCacheableShaderFromSourceFile(QOpenGLShader::Vertex,"vsrc.vert");
    program_.addCacheableShaderFromSourceFile(QOpenGLShader::Fragment,"fsrc.frag");
    program_.link();

    r_ = r;
    int angleSpan = 10; //弧度 = 角度 * PI / 180
    for(int vAngle = -90; vAngle < 90; vAngle = vAngle + angleSpan){ //生成球面顶点
        for(int hAngle = 0; hAngle <= 360; hAngle = hAngle + angleSpan){
            float x0 = r * ::cos(vAngle * PI / 180) * ::cos(hAngle * PI / 180);
            float y0 = r * ::cos(vAngle * PI / 180) * ::sin(hAngle * PI / 180);
            float z0 = r * ::sin(vAngle * PI / 180);

            float x1 = r * ::cos(vAngle * PI / 180) * ::cos((hAngle + angleSpan) * PI / 180);
            float y1 = r * ::cos(vAngle * PI / 180) * ::sin((hAngle + angleSpan) * PI / 180);
            float z1 = r * ::sin(vAngle * PI / 180);

            float x2 = r * ::cos((vAngle + angleSpan) * PI / 180) * ::cos((hAngle + angleSpan) * PI / 180);
            float y2 = r * ::cos((vAngle + angleSpan) * PI / 180) * ::sin((hAngle + angleSpan) * PI / 180);
            float z2 = r * ::sin((vAngle + angleSpan) * PI / 180);

            float x3 = r * ::cos((vAngle + angleSpan) * PI / 180) * ::cos(hAngle * PI / 180);
            float y3 = r * ::cos((vAngle + angleSpan) * PI / 180) * ::sin(hAngle * PI / 180);
            float z3 = r * ::sin((vAngle + angleSpan) * PI / 180);

            points_ << x1 << y1 << z1 << x3 << y3 << z3
                   << x0 << y0 << z0 << x1 << y1 << z1
                   << x2 << y2 << z2 << x3 << y3 << z3;
        }
    }

    texture_ = new QOpenGLTexture(img);
    vbo_.create();
    vbo_.bind();
    vbo_.allocate(points_.constData(),points_.count() * sizeof GLfloat);
}

void BallTextureRender::render(QOpenGLExtraFunctions *f, QMatrix4x4 &pMatrix, QMatrix4x4 &vMatrix, QMatrix4x4 &mMatrix, QVector3D &lightLocation, QVector3D &camera)
{
    f->glEnable(GL_DEPTH_TEST);
    f->glEnable(GL_CULL_FACE);
    program_.bind();
    vbo_.bind();
    f->glActiveTexture(GL_TEXTURE0 + 0);
    program_.setUniformValue("uPMatrix",pMatrix);
    program_.setUniformValue("uVMatrix",vMatrix);
    program_.setUniformValue("uMMatrix",mMatrix);
    program_.setUniformValue("uLightLocation",lightLocation);
    program_.setUniformValue("uCamera",camera);
    program_.setUniformValue("uR",r_);
    program_.setUniformValue("sTexture",0);
    program_.enableAttributeArray(0);
    program_.enableAttributeArray(1);

    texture_->bind(0);
    program_.setAttributeBuffer(0,GL_FLOAT,0,3,3 * sizeof GLfloat);
    program_.setAttributeBuffer(1,GL_FLOAT,0,3,3 * sizeof GLfloat);
    f->glDrawArrays(GL_TRIANGLES,0,points_.count() / 3);

    program_.disableAttributeArray(0);
    program_.disableAttributeArray(1);
    texture_->release();
    vbo_.release();
    program_.release();
    f->glDisable(GL_DEPTH_TEST);
    f->glDisable(GL_CULL_FACE);
}
